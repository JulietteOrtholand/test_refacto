from abc import ABC
import pandas as pd
import numpy as np
import json
import matplotlib.pyplot as plt
import torch

path_to_leaspype = f'../../leaspype'

# beware to update leaspy submodule with latest dev version
# command: `git submodule update --remote`
import os

path_to_leaspy = os.path.join(path_to_leaspype, 'leaspy')

import site

site.addsitedir(path_to_leaspype)
site.addsitedir(path_to_leaspy)

from leaspy import Leaspy, IndividualParameters, Dataset, Data


class SimulateData(ABC):

    def __init__(self, dict_param, join_type, name):
        assert join_type in ["NOISY_LINK", "NO_LINK"]
        self.parameters = {}
        self.load_parameters(dict_param)
        self.join_type = join_type
        self.features = ['ALSFRS_R_TOTAL']
        self.name = name

    ## --- SET PARAMETERS ---
    def load_parameters(self, dict_param):
        self.set_param_repeated_measure(dict_param['repeated_measure'])
        self.set_param_time_to_event(dict_param['time_to_event'])
        self.set_param_study(dict_param['study'])
        self.set_param_join(dict_param['join'])

    def save_parameters(self, path_save):
        total_params = {"repeated_measure": self.param_rm,
                        "time_to_event": self.param_event,
                        "study": self.param_study,
                        "join": self.param_join}
        with open(f"{path_save}params_simulated.json", 'w') as outfile:
            json.dump(total_params, outfile)

    def set_param_repeated_measure(self, dict_param):
        self.param_rm = {'g': dict_param['g'],
                         'v0': dict_param['v0'],
                         'tau_mean': dict_param['tau_mean'],
                         'tau_std': dict_param['tau_std'],
                         'xi_mean': dict_param['xi_mean'],
                         'xi_std': dict_param['xi_std'],
                         'noise_std': dict_param['noise_std'], }

    def set_param_time_to_event(self, dict_param):
        self.param_event = {'nu': dict_param['nu'],
                            'rho': dict_param['rho']
                            }

    def set_param_study(self, dict_param):
        self.param_study = {'pat_nb': dict_param['pat_nb'],
                            'fv_mean': dict_param['fv_mean'],
                            'fv_std': dict_param['fv_std'],
                            'tf_mean': dict_param['tf_mean'],
                            'tf_std': dict_param['tf_std'],
                            'distv_mean': dict_param['distv_mean'],
                            'distv_std': dict_param['distv_std'],
                            }

    def set_param_join(self, dict_param):
        self.param_join = {'delta_event': dict_param['delta_event'],
                           }

    ## ---- SIMULATE ---
    def simulate_data(self, seed=None):

        if seed is not None:
            np.random.seed(seed)

        # Simulate RE for RM
        df_ip_rm = self.get_ip_rm()

        # Simulate RE for survival
        df_ip_e = self.get_ip_e(df_ip_rm)

        # G
        self.get_leaspy_model()

        # Generate visits ages
        dict_timepoints, df_ind = self.generate_visit_ages(df_ip_rm)

        # Get visits observations
        df_long = self.generate_visits_observations(dict_timepoints, df_ip_rm)

        # Get event
        df_ip_e = self.get_time_to_event(df_ip_e)

        # Put everything in one dataframe
        df_ip_rm = df_ip_rm.rename(columns={'tau': 'RM_TAU',
                                        'xi': 'RM_XI'})
        df_rm = df_long.join(df_ip_rm, on='ID')
        df_ip_e = df_ip_e.rename(columns={'tau': 'E_TAU',
                                            'xi': 'E_XI'})
        df_sim = df_rm.join(df_ip_e, on='ID')

        # Drop too close visits
        #df_sim = df_sim.loc[df_sim.index.drop_duplicates()]

        # Apply censore
        df_censored = self.apply_censore(df_sim)

        # Select the number of patient needed
        id_pat = df_censored.index.get_level_values('ID').unique()[:self.param_study['pat_nb']]
        assert len(id_pat) == self.param_study['pat_nb']
        df_censored = df_censored.loc[id_pat]
        df_sim = df_sim.loc[id_pat]

        return df_sim, df_censored

    ## ---- IP ---
    def get_ip_rm(self):
        xi_rm = np.random.normal(self.param_rm["xi_mean"],
                                 self.param_rm["xi_std"],
                                 2*self.param_study['pat_nb'])

        tau_rm = np.random.normal(self.param_rm["tau_mean"],
                                         self.param_rm["tau_std"],
                                         2*self.param_study['pat_nb'])


        df_ip_rm = pd.DataFrame([xi_rm, tau_rm],
                              index=['xi', 'tau'],
                              columns=[str(i) for i in range(0, 2*self.param_study['pat_nb'])]).T
        return df_ip_rm

    def get_ip_e(self, df_ip_rm):

        if self.join_type == "NO_LINK":
            xi_e = np.random.normal(self.param_rm["xi_mean"],
                                    self.param_rm["xi_std"],
                                    2*self.param_study['pat_nb'])
            tau_e = np.random.normal(self.param_rm["tau_mean"],
                                     self.param_rm["tau_std"],
                                     2*self.param_study['pat_nb'])
            df_ip_e = pd.DataFrame([xi_e, tau_e],
                                   index=['xi', 'tau'],
                                   columns=[str(i) for i in range(0, 2*self.param_study['pat_nb'])]).T

        elif self.join_type == "NOISY_LINK":
            df_ip_e = df_ip_rm.copy()

        else:
            raise


        return df_ip_e

    ## ---- MODEL ---
    def get_leaspy_model(self):
        print(dict(**self.param_rm,**self.param_event))

        self.model = Leaspy('joint_univariate_logistic').load({'leaspy_version': '1.3.1',
                                                           'name': 'joint_univariate_logistic',
                                                           'features': self.features,
                                                           'noise_model': 'joint',
                                                           'parameters': dict(**self.param_rm,
                                                                              **self.param_event)})

    ## ---- RM ---
    def generate_visit_ages(self, df):

        df_ind = df.copy()

        ## Get age at baseline
        df_ind['AGE_AT_BASELINE'] =  df_ind['tau'] + np.random.normal(self.param_study['fv_mean'],
                                                                     self.param_study['fv_std'],
                                                                     2*self.param_study['pat_nb'])

        df_ind['AGE_FOLLOW_UP'] = df_ind['AGE_AT_BASELINE'] + np.random.normal(self.param_study['tf_mean'],
                                                                                           self.param_study['tf_std'],
                                                                                           2*self.param_study['pat_nb'])
        ## Generate visit ages for each patients
        dict_timepoints = {}
        for id_ in df_ind.index.values:

            ## Get the number of visit per patient
            time = df_ind.loc[id_, "AGE_AT_BASELINE"]
            age_visits = [time]
            while time < df_ind.loc[id_, "AGE_FOLLOW_UP"]:

                # Add one age at visit
                time += np.random.normal(self.param_study['distv_mean'],self.param_study['distv_std'])
                age_visits.append(time)

            dict_timepoints[id_] = list(age_visits)

        return dict_timepoints, df_ind

    def generate_visits_observations(self, dict_timepoints, df_ind):

        values = self.model.estimate(dict_timepoints, IndividualParameters().from_dataframe(df_ind[['xi', 'tau']]))
        print()
        df_long = pd.concat(
            [pd.DataFrame(values[id_][:,0] + np.random.normal(0, self.param_rm['noise_std'], values[id_][:,0].shape),
                          index=pd.MultiIndex.from_product([[id_], dict_timepoints[id_]], names=["ID", "TIME"]),
                          columns=self.features) for id_ in values.keys()])
        for feat in self.features:
            #df_long = df_long[df_long[feat]<1.]
            #df_long = df_long[df_long[feat] > 0.]
            df_long[feat] =df_long[feat].clip(0.0001,0.9999)
        return df_long

    ## ---- EVENTS ---
    def get_e_xi(self, df):

        def d1(nu, rho, T, xi):
            return (rho * np.exp(xi) / nu) * ((T * np.exp(xi) / nu) ** (rho - 1)) * np.exp(
                -(T * np.exp(xi) / nu) ** rho)

        def get_xi(t_obs, xi_possible, t_possible):
            xi_comp = xi_possible[abs(t_possible - t_obs).argmin()]
            return xi_comp

        # Init
        df_ind = df.copy()
        x = np.linspace(0, 200, 20000)
        xi_possible = np.linspace(-5, 5, 20000)

        # Get t associated to each xi_possible
        t_possible = np.array([x[d1(self.param_event['nu'],
                                    self.param_event['rho'],
                                    x, xi).argmax()] for xi in xi_possible])

        # Plot to check
        t_possible = np.array([x[d1(self.param_event['nu'],
                                    self.param_event['rho'], x, xi).argmax()] for xi in xi_possible])
        plt.plot(t_possible, xi_possible, color="red", label="xi = f(death)")
        plt.xlabel('Death time')
        plt.ylabel('xi')
        plt.title('Xi in function of death time')
        plt.legend()
        plt.show()

        # Extract xi from simulated t
        df_ind['E_XI'] = df_ind.apply(lambda x: get_xi(x['EVENT_DELAY'], xi_possible, t_possible), axis=1)

        # Correct mean
        print(f'The real average xi has been shifted of {df_ind["E_XI"].mean()}')
        df_ind['E_XI'].hist(density=True, color='grey', alpha=0.4, label='Extracted data')

        df_ind["E_XI"] = df_ind['E_XI'] + self.param_rm["xi_mean"]  # - df_ind["xi"].mean()

        df_ind["E_XI"].hist(density=True, color='red', alpha=0.4, label='Final data')
        plt.xlabel('xi')
        plt.ylabel('Density')
        plt.title('Xi')
        plt.legend()
        plt.show()

        return df_ind

    def density(self,x,nu, rho, xi, tau, tau_mean):
        T = np.clip((x-(tau)), a_min = 0, a_max = None)* np.exp(xi)
        return (rho * np.exp(xi) / nu) * ((T  / nu) ** (rho - 1)) * np.exp(-(T / nu) ** rho)

    def get_time_to_event(self, df_ip):


        # Initialization
        df_ip_e = df_ip.copy()

        def get_noisy_event(x):
            nu = np.exp(-self.param_event['nu'][0])
            rho = np.exp(self.param_event['rho'][0])
            xi = x['xi']
            tau = x['tau']
            sim = nu * np.random.weibull(rho)
            return sim/np.exp(xi) + tau

        df_ip_e['EVENT_NC_TIME'] = df_ip_e.apply(lambda x: get_noisy_event(x), axis=1)
        df_ip_e['EVENT_NC_BOOL'] = True

        return df_ip_e



    ## --- CENSORE ---

    def apply_censore(self, df):

        def censore_event(grp):
            age_event = grp['EVENT_NC_TIME'].values[0]
            if age_event <= max(grp.index.get_level_values('TIME')):
                grp['EVENT_TIME'] = grp['EVENT_NC_TIME'].values[0]
                grp['EVENT_BOOL'] = True
                grp = grp[grp.index.get_level_values('TIME') < age_event]
            else:
                grp['EVENT_TIME'] = max(grp.index.get_level_values('TIME'))
                grp['EVENT_BOOL'] = False

            return grp

        df_all = df.copy()

        df_all = df_all.reset_index()
        df_all = df_all[(df_all['TIME'] >= 0) & (df_all['EVENT_NC_TIME'] >= 0)]
        df_all = df_all.set_index(['ID', 'TIME'])

        df_censored = df_all.groupby('ID', group_keys=False).apply(censore_event)
        return df_censored