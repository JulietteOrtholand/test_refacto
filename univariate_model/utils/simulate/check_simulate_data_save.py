from abc import ABC, abstractmethod, abstractproperty
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from lifelines.datasets import load_diabetes
from lifelines.plotting import plot_interval_censored_lifetimes
from lifelines import (WeibullFitter, ExponentialFitter,
                       LogNormalFitter, LogLogisticFitter, NelsonAalenFitter,
                       PiecewiseExponentialFitter, GeneralizedGammaFitter, SplineFitter)
from lifelines import KaplanMeierFitter
from sklearn.linear_model import LinearRegression
import torch
from scipy import stats
import copy

path_to_leaspype = f'../leaspype'

# beware to update leaspy submodule with latest dev version
# command: `git submodule update --remote`
import os

path_to_leaspy = os.path.join(path_to_leaspype, 'leaspy')

import site

site.addsitedir(path_to_leaspype)
site.addsitedir(path_to_leaspy)

from leaspy import Leaspy, IndividualParameters, Dataset, Data, AlgorithmSettings
from leaspy.io.logs.visualization.plotting import Plotting
from leaspype.postprocessing_analysis.general.plots.average_trajectory import plot_average_trajectory_resampling



class CheckSimulateData(ABC):

    def __init__(self, simulator, path_out, seed):
        self.simulator = simulator
        self.df_all, self.df_censored = simulator.simulate_data(seed)
        self.path_out = path_out + f"{self.simulator.name}/"
        self.graph_all = {'color': 'blue',
                          'label': "All"}
        self.graph_censored = {'color': 'orange',
                               'label': "Censored"}
        self.graph_theo = {'color': 'green',
                           'label': "Theory"}
        self.data ={}
        self.models = {}
        self.rm_rec = {}
        self.ip_rec = {}
        self.ip_mcmc = {}
        if not os.path.isdir(self.path_out):
            os.makedirs(self.path_out)

    def check_all(self):

        self.check_all_parameters()
        self.check_all_survival()
        self.check_all_repeated_measures()

        for id_pat in self.df_censored.index.get_level_values('ID').unique()[:5]:
            self.check_individual(id_pat)

    ## --- CHECK PARAMETERS ---
    def check_all_parameters(self):

        self.simulator.save_parameters(self.path_out)
        self.check_individual_parameters()
        self.check_event_density()
        self.check_rep_event_density()
        self.check_number_visits()
        self.check_distance_between_visits()
        self.check_first_visit_parameters()
        self.check_time_follow_up()

        #if self.simulator.join_type == 'NOISY_LINK':
        #    self.check_link_parameters()


    def get_normal(self, x, mean, std):
        return 1 / (std * np.sqrt(2 * np.pi)) * np.exp((-1 / 2) * ((x - mean) / std) ** 2)

    def check_individual_parameters(self):
        # TODO: Add mean in graphs
        for param, param_name in zip(['RM_XI', 'RM_TAU', 'E_XI', 'E_TAU'], ["xi", "tau", "xi", "tau"]):
            self.df_all.groupby('ID').first()[param].hist(density=True, alpha=0.4, **self.graph_all)
            self.df_censored.groupby('ID').first()[param].hist(density=True, alpha=0.4, **self.graph_censored)

            x = np.arange(int(self.df_all[param].min()), int(self.df_all[param].max()) + 1, 0.1)
            plt.plot(x, self.get_normal(x, self.simulator.param_rm[f"{param_name}_mean"],
                                        self.simulator.param_rm[f"{param_name}_std"]),
                     **self.graph_theo)
            plt.xlabel(param)
            plt.ylabel('Density')
            plt.title(f"{param} on {self.simulator.name}")
            plt.legend()
            plt.savefig(f"{self.path_out}param_{param}_distrib.png")
            plt.show()

    def check_event_density(self):
        def f_f(x, nu, rho):
            T = np.clip((x), a_min = 0, a_max = None)
            return (rho / nu) * ((T / nu) ** (rho - 1)) * np.exp(-(T / nu) ** rho)

        # Plot data
        plt.hist(self.df_all.groupby('ID').first()["EVENT_NC_TIME"]-self.df_all.groupby('ID').first()["E_TAU"],
                 density=True, label="All", bins=50,color='blue', alpha=0.4)
        plt.hist(self.df_censored.groupby('ID').first()["EVENT_TIME"]-self.df_censored.groupby('ID').first()["E_TAU"],
                 density=True, label="Censored", bins=50,color='orange', alpha=0.4)

        time = np.arange(0,25, 0.1)#self.df_all["EVENT_NC_TIME"].min(), self.df_all["EVENT_NC_TIME"].max(), 0.1)
        plt.plot(time, f_f(time,
                           np.exp(-self.simulator.param_event['nu'][0]),
                           np.exp(self.simulator.param_event['rho'][0])),
                 label="Theory", color='green')
        plt.ylabel("Death distribution $f(t)$")
        plt.xlabel("Age")
        plt.title(f'Time to death  on {self.simulator.name}')
        plt.legend()
        plt.savefig(f"{self.path_out}param_event_density.png")
        plt.show()

    def check_rep_event_density(self):
        def f_f(x, nu, rho):
            T = np.clip((x), a_min = 0, a_max = None)
            return (rho / nu) * ((T / nu) ** (rho - 1)) * np.exp(-(T / nu) ** rho)

        # Plot data
        df_surv_all = self.df_all.groupby('ID').first()
        df_surv_censored = self.df_censored.groupby('ID').first()
        s_rep_censored = (df_surv_censored["EVENT_NC_TIME"] - (df_surv_censored["E_TAU"])) * (np.exp(df_surv_censored["E_XI"]))
        # estimated weibull
        wb= stats.exponweib.fit( (df_surv_censored["EVENT_NC_TIME"] - (df_surv_censored["E_TAU"])) * (np.exp(df_surv_censored["E_XI"])),
                    floc=0, f0=1)
        plt.hist((df_surv_all["EVENT_NC_TIME"] - (df_surv_all["E_TAU"])) * (np.exp(df_surv_all["E_XI"])),
                 density=True, label=f"All  ({len(df_surv_all)})", bins=50,color='blue', alpha=0.4)
        plt.hist(s_rep_censored,
                 density=True, label=f"Censored ({len(s_rep_censored)})", bins=50,color='orange', alpha=0.4)

        time = np.arange(0,8, 0.1)#self.df_all["EVENT_NC_TIME"].min(), self.df_all["EVENT_NC_TIME"].max(), 0.1)
        plt.plot(time, f_f(time,
                           np.exp(-self.simulator.param_event['nu'][0]),
                           np.exp(self.simulator.param_event['rho'][0])),
                 label=f"Theory {round(np.exp(-self.simulator.param_event['nu'][0]),2),round(np.exp(self.simulator.param_event['rho'][0]),2)}", color='green')
        plt.plot(time, f_f(time,
                           wb[3],
                           wb[1]),
                 label= f"Censored Estimated {round(wb[3],2),round(wb[1],2)}", color='black')
        plt.ylabel("Death distribution $f(t)$")
        plt.xlabel("Age")
        plt.title(f'Reparametrized time to death  on {self.simulator.name}')
        plt.legend()
        plt.savefig(f"{self.path_out}param_rep_event_density.png")
        plt.show()

    def check_number_visits(self):
        x = np.arange(0, 20, 0.1)

        fig, axes = plt.subplots(1, 1, figsize=(7, 4))
        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_visit = df.reset_index("TIME").groupby("ID").count()[["TIME"]]
            df_visit.hist(**graph_set_up, density=True, alpha=0.4, ax=axes)

        plt.title(f"Number of visits per patients  on {self.simulator.name}")
        plt.xlabel("Density")
        plt.ylabel("Nb of patients")
        plt.legend()
        plt.savefig(f"{self.path_out}param_nb_visits.png")
        plt.show()

    def check_time_follow_up(self):

        x = np.arange(0, 5, 0.1)

        fig, axes = plt.subplots(1, 1, figsize=(7, 4))
        plt.plot(x, self.get_normal(x, self.simulator.param_study['tf_mean'],
                                    self.simulator.param_study['tf_std']), **self.graph_theo)
        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_dif = df.reset_index('TIME').groupby('ID')['TIME']

            (df_dif.max() - df_dif.min()).hist(**graph_set_up, density=True, alpha=0.4, ax=axes)

        plt.title(f"Follow up per patients on {self.simulator.name}")
        plt.xlabel("Density")
        plt.ylabel("Follow up (y)")
        plt.legend()
        plt.savefig(f"{self.path_out}follow_up.png")
        plt.show()

    def check_distance_between_visits(self):
        def visit_frequency(grp):
            df_freq = (grp.reset_index()[["TIME"]].diff()) * 12
            df_freq.columns = ["VISIT_FREQUENCY"]
            df_freq.index = grp.index
            grp["VISIT_FREQUENCY"] = df_freq["VISIT_FREQUENCY"]
            return grp

        x = np.arange(0, 6, 0.1)

        fig, axes = plt.subplots(1, 1, figsize=(7, 4))
        plt.plot(x, self.get_normal(x, 12 * self.simulator.param_study['distv_mean'],
                                    12 * self.simulator.param_study['distv_std']), **self.graph_theo)
        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_freq = df.groupby("ID", group_keys=True).apply(visit_frequency)
            df_freq = df_freq.dropna(subset=["VISIT_FREQUENCY"])[["VISIT_FREQUENCY"]]
            df_freq.hist(density=True, alpha=0.4, **graph_set_up, ax=axes)

        # plt.xlim(0, 5)
        plt.title(f"Time between visits (mo.) on {self.simulator.name}")
        plt.xlabel("Months")
        plt.ylabel("Density")
        plt.legend()
        plt.savefig(f"{self.path_out}param_dist_btw_visits.png")
        plt.show()

    def check_link_parameters(self):


        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_origin = df.groupby('ID').first()
            df_origin['DELTA'] = df_origin['E_TAU'] - df_origin['RM_TAU']
            df_origin['DELTA'].hist(density=True, alpha=0.4, **graph_set_up)

        x = np.arange(int(df_origin['DELTA'].min()) - 1, int(df_origin['DELTA'].max()) + 1, 0.1)
        plt.plot(x, self.get_normal(x, 0,
                                    self.simulator.param_join[f"origin_std"]),
                 **self.graph_theo)
        plt.xlabel('Tau delta')
        plt.ylabel('Density')
        plt.title(f'Tau difference between E and RM on {self.simulator.name}')
        plt.legend()
        plt.savefig(f"{self.path_out}param_link_tau_distrib.png")
        plt.show()

        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_origin = df.groupby('ID').first()
            df_origin['DELTA'] = df_origin['E_XI'] - df_origin['RM_XI']
            df_origin['DELTA'].hist(density=True, alpha=0.4, **graph_set_up)

        x = np.arange(int(df_origin['DELTA'].min()) - 1, int(df_origin['DELTA'].max()) + 1, 0.1)
        plt.plot(x, self.get_normal(x, 0, self.simulator.param_join[f"alpha_std"]),
                 **self.graph_theo)
        plt.xlabel('Xi delta')
        plt.ylabel('Density')
        plt.title(f'Xi difference between E and RM on {self.simulator.name}')
        plt.legend()
        plt.savefig(f"{self.path_out}param_link_xi_distrib.png")
        plt.show()

    def check_first_visit_parameters(self):
        # TODO: Add mean in graphs
        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            df_origin = df.reset_index().groupby('ID').min()
            df_origin['DELTA'] = df_origin['TIME'] - df_origin['RM_TAU']
            df_origin['DELTA'].hist(density=True, alpha=0.4, **graph_set_up)

        x = np.arange(int(df_origin['DELTA'].min()) - 1, int(df_origin['DELTA'].max()) + 1, 0.01)
        plt.plot(x, self.get_normal(x, self.simulator.param_study[f"fv_mean"],
                                    self.simulator.param_study[f"fv_std"]),
                 **self.graph_theo)
        plt.xlabel(f'Delta first visit compared to tau on {self.simulator.name}')
        plt.ylabel('Density')
        plt.title('Delta first visit')
        plt.legend()
        plt.savefig(f"{self.path_out}param_fv_distrib.png")
        plt.show()

    ## --- CHECK SURVIVAL ---

    def check_all_survival(self):
        self.check_surv_pred_error()
        self.check_nc_survival_all()
        self.check_survival_censored()
        self.check_rep_survival_censored()

    def f_s(self, x, rho, nu, tau_mean):
        print(rho, nu, tau_mean)
        T = np.clip((x-tau_mean)* np.exp(self.df_censored.groupby('ID').first()["RM_XI"].mean()), a_min = 0, a_max = None)
        return np.exp(-(T / np.exp(-nu)) ** np.exp(rho))

    def check_surv_pred_error(self):

        # Init
        times = np.arange(0, 200, 0.001)
        xi_mean = self.simulator.param_rm['xi_mean']
        df_bool = self.df_censored.groupby('ID').first()['EVENT_BOOL']
        perc_censure = int(100 * df_bool.value_counts()[False] / len(df_bool))


        for xi_col, tau_col in [("E_XI", "E_TAU"), ("RM_XI", "RM_TAU")]:
            fig, axes = plt.subplots(1, 1, figsize=(7, 4))
            df_all_cut = self.df_all[(self.df_all.index.get_level_values('TIME') >= 0) & (self.df_all['EVENT_NC_TIME'] >= 0)]
            for df, graph_set_up in zip([df_all_cut, self.df_censored], [self.graph_all, self.graph_censored]):

                df_surv = df.groupby('ID').first()

                # Get prediction
                df_rep = df_surv.apply(lambda x: times[np.nanargmax(self.simulator.density(times,
                                                               np.exp(-self.simulator.param_event['nu'][0]),
                                                               np.exp(self.simulator.param_event['rho'][0]),
                                                               x[xi_col],
                                                               x[tau_col],
                                                               self.simulator.param_rm["tau_mean"]))],
                                   axis=1).to_frame(name='T_PRED')
                # Get real values
                df_rep["T_REAL"] = df_surv['EVENT_NC_TIME']
                # Plot
                (df_rep["T_REAL"] - df_rep["T_PRED"]).hist(ax=axes, alpha=0.4, color = graph_set_up['color'],
                                                           label = f"{xi_col.split('_')[0]} {graph_set_up['label']}",
                                                           density=True)

            plt.ylabel("Survival distribution")
            plt.xlabel("Time")
            plt.title(f"Time-to-event prediction error with {xi_col.split('_')[0]} parameters on {self.simulator.name}"
                      f"\n {perc_censure}% of event censure")
            plt.legend()
            plt.savefig(f"{self.path_out}tte_{xi_col.split('_')[0]}_pred_error.png")
            plt.show()

    def check_surv_plot(self, df, col_t, col_b):

        # Init
        time = np.arange(0, 20, 0.1)
        fig, axes = plt.subplots(1, 1, figsize=(7, 4))

        # Plot analysis
        kmf = KaplanMeierFitter().fit(df[col_t], df[col_b])
        kmf.plot_survival_function(ax=axes, label="Kaplan Meier", color="brown")

        # Plot analysis
        wbf = WeibullFitter().fit(df[col_t], df[col_b])
        wbf.plot_survival_function(ax=axes, label=f"Lifeline Weibull: W{(round(wbf.rho_, 2), round(wbf.lambda_, 2))}",
                                   color="orange")

        plt.plot(time, self.f_s(time,
                                self.simulator.param_event['rho'][0],
                                self.simulator.param_event['nu'][0] ,
                                self.df_censored.groupby('ID').first()["RM_TAU"].mean()),
                 label=f"Theoretical: W{round(np.exp(self.simulator.param_event['rho'][0]),2), round(np.exp(-self.simulator.param_event['nu'][0]),2)} + "
                       f"{round(self.simulator.param_rm['tau_mean'],2)}")
        return axes

    def check_rep_survival_censored(self):
        # Init
        df = self.df_censored.groupby('ID').first()
        df['REP_TIME'] = (df["EVENT_NC_TIME"] - (df["E_TAU"])) * (np.exp(df["E_XI"])) + self.df_censored.groupby('ID').first()["RM_TAU"].mean()
        df["EVENT_NC_BOOL"] = True

        # plot
        self.check_surv_plot(df, "REP_TIME", "EVENT_NC_BOOL")

        #
        """val = np.arange(0, 100, 0.001)


        surv_mean = val[np.nanargmax(self.simulator.density(val,
                                                            np.exp(-self.simulator.param_event['nu'][0]),
                                                            np.exp(self.simulator.param_event['rho'][0]),
                                                  0.,
                                                  x["E_TAU"],
                                                  self.simulator.param_rm['tau_mean']))]
        plt.plot([surv_mean, surv_mean], [0, 1], linestyle='dashed', label='theo_mean')

        surv_mean = val[np.nanargmax(self.simulator.density(val,
                                                            np.exp(-self.simulator.param_event['nu'][0]),
                                                            np.exp(self.simulator.param_event['rho'][0]),
                                                  df['E_XI'].mean() - self.simulator.param_rm['xi_mean'],
                                                  x["E_TAU"],
                                                  self.simulator.param_rm['tau_mean']))]
        plt.plot([surv_mean, surv_mean], [0, 1], linestyle='dashed', label='real_mean')"""

        # Titles
        plt.ylabel("Survival distribution")
        #plt.xlim((0, 20))
        plt.xlabel("Time")
        plt.title(f"Not censored survival over all patients on {self.simulator.name}")
        plt.legend()
        plt.savefig(f"{self.path_out}tte_surv_all.png")
        plt.show()

    def check_nc_survival_all(self):
        # OK
        # Init
        df_all_cut = self.df_all[(self.df_all.index.get_level_values('TIME') >= 0) & (self.df_all['EVENT_NC_TIME'] >= 0)]
        df = df_all_cut.groupby('ID').first()
        df["EVENT_NC_BOOL"] = True

        # plot
        self.check_surv_plot(df, "EVENT_NC_TIME", "EVENT_NC_BOOL")

        # Titles
        plt.ylabel("Survival distribution")
        #plt.xlim((0, 20))
        plt.xlabel("Time")
        plt.title(f"Not censored survival over all patients on {self.simulator.name}")
        plt.legend()
        plt.savefig(f"{self.path_out}tte_surv_all.png")
        plt.show()

    def get_mode(self, nu, rho):
        return nu *((rho-1)/rho)**(1/rho)

    def check_survival_censored(self):
        # Init
        df = self.df_censored.groupby('ID').first()
        df["EVENT_NC_BOOL"] = True
        df_bool = self.df_censored.groupby('ID').first()['EVENT_BOOL']
        perc_censure = int(100 * df_bool.value_counts()[False] / len(df_bool))

        # plot
        axes = self.check_surv_plot(df, "EVENT_TIME", "EVENT_BOOL")

        # Add not censored
        kmf = KaplanMeierFitter().fit(df["EVENT_NC_TIME"], df["EVENT_NC_BOOL"])
        kmf.plot_survival_function(ax=axes, label="Not censored Kaplan Meier", color="grey")

        # Add from prediction
        val = np.linspace(0, 20, 20000)
        rho, nu = np.exp(self.simulator.param_event['rho'][0]), np.exp(-self.simulator.param_event['nu'][0])
        mode = nu*((rho-1)/rho)**(1/rho)
        xi_mean = self.simulator.param_rm['xi_mean']
        # From event individual parameters
        kmf = KaplanMeierFitter().fit(
            #df.apply(lambda x: val[np.nanargmax(self.simulator.density(val,
            #                                                np.exp(-self.simulator.param_event['nu'][0]),
            #                                                np.exp(self.simulator.param_event['rho'][0]),
            #                                                 x["E_XI"] - xi_mean,
            #                                                 x["E_TAU"],
            #                                                 self.simulator.param_rm['tau_mean']))],
            #         axis=1),
            df.apply(
                lambda x: self.simulator.param_rm['tau_mean'] + np.exp(x['E_XI']) * (x['EVENT_NC_TIME'] - (x['E_TAU'])),
                axis=1),
            df["EVENT_NC_BOOL"])
        kmf.plot_survival_function(ax=axes, label="E predicted events", color="blue")

        # From RM individual parameters
        kmf = KaplanMeierFitter().fit(
            #df.apply(lambda x: val[np.nanargmax(self.simulator.density(val,
            #                                                np.exp(-self.simulator.param_event['nu'][0]),
            #                                                np.exp(self.simulator.param_event['rho'][0]),
            #                                                 x["RM_XI"] - xi_mean,
            #                                                 x["RM_TAU"],
            #                                                 self.simulator.param_rm['tau_mean']))],
            #         axis=1),
            df.apply(
                lambda x: self.simulator.param_rm['tau_mean'] + np.exp(x['RM_XI']) * (x['EVENT_NC_TIME'] - (x['RM_TAU'])),
                axis=1),

            df["EVENT_NC_BOOL"])
        kmf.plot_survival_function(ax=axes, label="RM predicted events", color="pink")

        # Titles
        plt.ylabel("Survival distribution")
        #plt.xlim((0, 20))
        plt.xlabel("Time")
        plt.title(f"Survival over censored patients on {self.simulator.name}"
                  f"\n {perc_censure}% of event censure")
        plt.legend()
        plt.savefig(f"{self.path_out}tte_surv_censored.png")
        plt.xlim(0,20)
        plt.show()

    ## --- CHECK REPEATED MEASURES ---

    def check_all_repeated_measures(self):
        self.check_visits()
        self.check_rm_modelling()
        self.plot_rep_RM()

    def check_visits(self):

        for df, graph_set_up in zip([self.df_all, self.df_censored], [self.graph_all, self.graph_censored]):
            for value_name in self.simulator.features:
                fig, axes = plt.subplots(1, 1, figsize=(4, 3))
                plt.scatter(df.index.get_level_values("TIME"), df[value_name], alpha=0.4, **graph_set_up)
                plt.title(
                    f"{graph_set_up['label']} {value_name} visits  on {self.simulator.name}\n ({len(df.reset_index()['ID'].unique())} patients, {len(df)} visits)")
                plt.ylim((-0.1, 1.1))
                plt.savefig(f"{self.path_out}rm_visits_{graph_set_up['label']}_{value_name}.png", bbox_inches='tight')
                plt.show()

    def get_models(self):
        # Get models
        params = copy.copy(self.simulator.param_rm)
        params['xi_mean'] = params['v0']
        self.models['theo'] = Leaspy('univariate_logistic').load({'leaspy_version': '1.3.1',
                                                                               'name': 'univariate_logistic',
                                                                               'features': self.simulator.features,
                                                                               'noise_model': 'gaussian_scalar',
                                                                               'parameters': params})
        self.data['all'], self.models['all'] = self.train_reconstruct_model(self.df_all, f"{self.path_out}log_all")
        self.data['theo'] = self.data['all']
        self.data['censored'], self.models['censored'] = self.train_reconstruct_model(self.df_censored,
                                                                                      f"{self.path_out}log_censored")

    def check_rm_modelling(self):

        # Get models
        self.get_models()

        # Get ip mcmc
        self.ip_mcmc['all'] = self.get_one_ip_mcmc(self.data['all'], f"{self.path_out}log_all")
        self.ip_mcmc['censored'] = self.get_one_ip_mcmc(self.data['censored'], f"{self.path_out}log_censored")

        # Get ips
        self.ip_rec['all'] = self.get_ip(self.df_all, self.models['all'])
        self.ip_rec['theo'] = self.get_ip(self.df_all, self.models['theo'])
        self.ip_rec['censored'] = self.get_ip(self.df_censored, self.models['censored'])
        # Get estimates
        self.rm_rec['all'] = self.get_est(self.df_all, self.ip_rec['all'], 'all')
        self.rm_rec['theo'] = self.get_est(self.df_all, self.ip_rec['theo'], 'theo')
        self.rm_rec['censored'] = self.get_est(self.df_censored, self.ip_rec['censored'], 'censored')

        # Plots
        self.plot_avg_models()
        self.plot_ind_param()

    def plot_avg_models(self):

        # Plot average models
        fig, axes = plt.subplots(1, 1, figsize=(8, 4))
        axes = self.plot_average_model(self.models['theo'], self.graph_theo, axes=axes)
        axes = self.plot_average_model(self.models['all'], self.graph_all, axes=axes)
        axes = self.plot_average_model(self.models['censored'], self.graph_censored, axes=axes)
        axes.legend(["Theory", "","tau_theo", "All",  "", "tau_all", "Censored",  "", "tau_censored"])
        axes.set_title(f"Repeated measure model learnt  on {self.simulator.name}")
        plt.savefig(f"{self.path_out}rm_visits_models.png")
        plt.show()

    def plot_ind_param(self):

        for col, min_, max_ in zip(["xi", "tau"], [-3, 20], [-2, 20]):
            f, ax = plt.subplots(figsize=(5, 5))
            if col == 'tau':
                ax.plot([min_- self.simulator.param_rm['tau_mean'],
                         max_- self.simulator.param_rm['tau_mean']],
                        [min_ ,
                         max_ ], color="green",
                    linestyle='dashed', label=f"y = x + {round(self.simulator.param_rm['tau_mean'],2)}")
            else:
                ax.plot([min_, max_], [min_, max_], color="green",
                        linestyle='dashed', label="y = x")

            plt.scatter(self.ip_rec['all'][f"RM_{col.upper()}"].values.T ,
                        self.ip_rec['all'][col].values.T,
                        alpha=0.4, **self.graph_all)
            plt.scatter(self.ip_rec['censored'][f"RM_{col.upper()}"].values.T,
                        self.ip_rec['censored'][col].values.T,
                        alpha=0.4, **self.graph_censored)

            lin_reg = LinearRegression().fit(self.ip_rec['censored'][[f"RM_{col.upper()}"]].values,
                                             self.ip_rec['censored'][col].values.T)
            ax.plot([min_, max_],
                    [lin_reg.predict(np.array([[min_]])), lin_reg.predict(np.array([[max_]]))],
                    color="orange",
                    label=f"y={round(lin_reg.coef_[0], 2)}*x+{round(lin_reg.intercept_, 2)}")

            ax.text(.75, .10,
                    f'$R^2$:{round(lin_reg.score(self.ip_rec["censored"][[f"RM_{col.upper()}"]].values, self.ip_rec["censored"][col].values.T), 2)}',
                    ha='left', va='top', transform=ax.transAxes, fontsize=15)

            plt.title(f"{col} on {self.simulator.name}")
            plt.xlabel(f"Real {col}")
            plt.ylabel(f"Univ {col}")
            plt.legend()
            plt.savefig(f"{self.path_out}rm_{col}_against_real.png", bbox_inches='tight')
            plt.show()

    def train_reconstruct_model(self, df, log_save):
        seed_value = 0
        print(df.dropna(how='all').sort_index()[self.simulator.features].index.is_unique)
        data_ready = Data.from_dataframe(df[self.simulator.features])

        algo_settings = AlgorithmSettings('mcmc_saem',
                                          n_iter=10000,  # n_iter defines the number of iterations
                                          seed=seed_value,  # to make the calibration deterministic for reproducibility
                                          sampler_type="FastGibbs")

        algo_settings.set_logs(
            path=log_save,  # Creates a logs file ; if existing, ask if rewrite it
            # plot_periodicity=50,  # Saves the values to display in pdf every 50 iterations
            save_periodicity=100,  # Saves the values in csv files every 10 iterations
            console_print_periodicity=None,  # If = N, it display logs in the console/terminal every N iterations
            overwrite_logs_folder=True,  # Default behaviour raise an error if the folder already exists.
        )

        model_ready = Leaspy('univariate_logistic',
                             # noise_model='gaussian_scalar' # estimate the residual noise scaling per feature
                             )

        model_ready.fit(data_ready, settings=algo_settings)
        print(model_ready.model.parameters)
        return data_ready, model_ready

    def get_ip(self, df, model_ready):

        seed_value = 0
        data_ready = Data.from_dataframe(df[self.simulator.features])
        # IP
        settings_personalization = AlgorithmSettings('scipy_minimize', use_jacobian=True, seed=seed_value)
        ip = model_ready.personalize(data_ready, settings_personalization)
        df_ip_rec = ip.to_dataframe()
        df_ip_rec = df_ip_rec.join(df.groupby('ID').first()[["RM_XI", "RM_TAU"]])

        return df_ip_rec

    def get_one_ip_mcmc(self, data_train, path_logs):

        path_re_folder = f"{path_logs}/parameter_convergence/"

        # Extract individual data
        id_pat = list(data_train.individuals.keys())

        dict_last_past = {}

        for file in ["xi", "tau"]:
            path_mcmc = f"{path_re_folder}{file}.csv"

            # Load data
            df_mcmc = pd.read_csv(path_mcmc, header=None)
            df_mcmc = df_mcmc.set_index([0])
            df_mcmc.columns = id_pat
            if "sources" in file:
                name_dict = f"sources_{file[-1]}"
            else:
                name_dict = file
            dict_last_past[name_dict] = df_mcmc.iloc[-1].T.to_dict()

        dict_torch = {}

        for ki in dict_last_past["xi"].keys():
            dict_torch[ki] = {}
            for kp in ["xi", "tau"]:
                dict_torch[ki][kp] = torch.tensor([dict_last_past[kp][ki]]).T

        df_ip_mcmc = pd.DataFrame(dict_torch).astype(float).T

        df_ip_mcmc.index.name = 'ID'
        return df_ip_mcmc

    def get_est(self, df_in, df_ip, model_name):

        # Get timepoints for each patient
        dict_ = df_in.groupby(level=0).apply(lambda df_in: df_in.xs(df_in.name).to_dict()).to_dict()
        dict_timepoints = {id_pat: list(dict_als['ALSFRS_R_TOTAL'].keys()) for id_pat, dict_als in dict_.items()}

        # Compute estimate
        res = self.models[model_name].estimate(dict_timepoints,
                                 IndividualParameters().from_dataframe(df_ip[['xi', 'tau']]))

        # Put to right format
        df_est = pd.concat([pd.DataFrame(np.array([val[:, 0]]).T,
                                         columns=pd.MultiIndex.from_product(
                                             [['predicted_norm'], ['ALSFRS_R_TOTAL']],
                                             names=["ID", "TIME"]),
                                         index=pd.MultiIndex.from_product([[key], dict_timepoints[key]],
                                                                          names=["ID", "TIME"])) for key, val in
                            res.items()])

        # Get classic values
        df_est[('true_norm', 'ALSFRS_R_TOTAL')] = df_in['ALSFRS_R_TOTAL']
        df_est[('error_norm', 'ALSFRS_R_TOTAL')] = df_est[('predicted_norm', 'ALSFRS_R_TOTAL')] - df_est[
            ('true_norm', 'ALSFRS_R_TOTAL')]
        df_est[('error_absolute_norm', 'ALSFRS_R_TOTAL')] = abs(df_est[('error_norm', 'ALSFRS_R_TOTAL')])

        return df_est

    def plot_average_model(self, model, graph_set_up, df_ip = [], axes=None):

        if axes == None:
            fig, axes = plt.subplots(1, 1, figsize=(8, 4))

        if len(df_ip) == 0:
            ip = None
        else:
            ip = [IndividualParameters().from_dataframe(df_ip[['xi', 'tau']])]
        # All
        plot_average_trajectory_resampling([model],
                                           ip, #,
                                           np.arange(0, 20, 1),
                                           ax=axes, color=[graph_set_up['color']],
                                           labels=[graph_set_up['label']])

        axes.plot([model.model.parameters['tau_mean'],
                   model.model.parameters['tau_mean']],
                  [0, 1], linestyle='dashed', color=graph_set_up['color'], label=f"tau {graph_set_up['label']}")
        return axes

    def plot_rep_RM(self):

        df_rep = self.df_censored.join(self.ip_mcmc['censored'][['xi', 'tau']]).reset_index('TIME')
        df_rep['REP_TIME'] = (df_rep['TIME'] - df_rep['tau']) * np.exp(df_rep['xi']) + float(
            self.models['censored'].model.parameters['tau_mean'])
        df_rep = df_rep.set_index('REP_TIME', append=True)

        df_ip_theo  = self.df_censored.groupby('ID').first()[['RM_XI', 'RM_TAU']]
        df_ip_theo = df_ip_theo.rename(columns = {'RM_XI':'xi', 'RM_TAU':'tau'})
        fig, axes = plt.subplots(1, 1, figsize=(8, 4))


        # Average models
        self.plot_average_model(self.models['theo'], {'color': 'green',
                           'label': "Theo"}, axes=axes)
        self.plot_average_model(self.models['theo'], {'color': 'red',
                                                      'label': "Theo corrected"},df_ip = df_ip_theo, axes=axes)

        self.plot_average_model(self.models['censored'], {'color': 'black',
                           'label': "No corection"}, axes=axes)
        self.plot_average_model(self.models['censored'], {'color': 'orange',
                           'label': "Reconstructed"}, df_ip = self.ip_rec['censored'], axes=axes)

        # Patients
        for id_ in df_rep.index.get_level_values('ID').unique():
            df_rep.loc[id_]['ALSFRS_R_TOTAL'].plot(alpha = 0.2)

        plt.xlim(0,20)
        #axes.legend(["Censored", "tau_censored", "UP", "tau_UP"])
        axes.set_title(f"Repeated measure model learnt with MCMC reparametrized visits \n on {self.simulator.name}")
        plt.savefig(f"{self.path_out}rm_rep_visits_models.png")
        plt.show()


    ## --- CHECK INDIVIDUALS ---
    def check_individual(self, id_pat):

        path_pat = self.path_out + "patients/"
        if not os.path.isdir(path_pat):
            os.makedirs(path_pat)


        # TODO
        leaspy = Leaspy('univariate_logistic').load({'leaspy_version': '1.3.1',
                                                     'name': 'univariate_logistic',
                                                     'features': self.simulator.features,
                                                     'noise_model': 'gaussian_scalar',
                                                     'parameters': self.simulator.param_rm})

        leaspy = Leaspy('joint_univariate_logistic').load({'leaspy_version': '1.3.1',
                                                  'name': 'joint_univariate_logistic',
                                                  'features': self.simulator.features,
                                                  'noise_model': 'joint',
                                                  'parameters': dict(**self.simulator.param_rm,
                                                                     **self.simulator.param_event)})

        pat_censored = self.df_censored.loc[id_pat]
        pat_all = self.df_all.loc[id_pat]
        pat_cov = self.df_censored.groupby('ID').first().rename(columns={"RM_XI": 'xi', "RM_TAU": 'tau'}).loc[
            id_pat]

        ages_pat = np.arange(int(pat_all.index.get_level_values('TIME').min()),
                             int(pat_all.index.get_level_values('TIME').max()) + 1.5,
                             0.1)

        values = leaspy.estimate({id_pat: ages_pat},
                                 IndividualParameters().from_dataframe(pat_cov.to_frame().T[['xi', 'tau']]))
        plt.plot(ages_pat, values[id_pat][:,0],
                 label="simulate", color="purple")

        values = leaspy.estimate({'mean_pat': ages_pat},
                                 IndividualParameters().from_pytorch(['mean_pat'],
                                                                     {"tau": torch.tensor([[pat_cov['tau']]]),
                                                                      "xi": torch.tensor(
                                                                          [[self.simulator.param_rm['xi_mean']]]), }))
        plt.plot(ages_pat, values['mean_pat'][:,0],
                 label="mean_pat", color="green", linestyle='dashed', )

        plt.scatter(pat_censored.index.get_level_values("TIME"),
                    pat_censored[self.simulator.features],
                    label="visits", color="black")

        if pat_cov["EVENT_BOOL"]:
            plt.plot([pat_cov["EVENT_TIME"],
                      pat_cov["EVENT_TIME"] ], [0, 1], color="red",
                     label="event")

            df_vc = pat_all.drop(pat_censored.index)
            plt.scatter(df_vc.index.get_level_values("TIME"),
                        df_vc[self.simulator.features],
                        label="censored visits", color="black", facecolors='none')

        else:
            plt.plot([pat_cov["EVENT_TIME"],
                      pat_cov["EVENT_TIME"]], [0, 1], color="black",
                     linestyle='dashed', label="censure")
            plt.plot([pat_cov["EVENT_NC_TIME"],
                      pat_cov["EVENT_NC_TIME"]], [0, 1], color="grey",
                     label="event")

        plt.legend()
        plt.ylim([0, 1])
        plt.ylabel("score")
        plt.xlabel("time")
        plt.title(
            f'Patient {id_pat} \n {round(pat_cov["xi"], 2), round(pat_cov["tau"], 2)}')
        plt.savefig(f"{path_pat}{id_pat}_rm.png")
        plt.show()

        f, ax = plt.subplots()

        if pat_cov["EVENT_BOOL"]:
            plt.plot([pat_cov["EVENT_TIME"],
                      pat_cov["EVENT_TIME"]], [0, 1], color="red",
                     label="event")

        else:
            plt.plot([pat_cov["EVENT_TIME"],
                      pat_cov["EVENT_TIME"]], [0, 1], color="black",
                     linestyle='dashed', label="censure")
            plt.plot([pat_cov["EVENT_NC_TIME"],
                      pat_cov["EVENT_NC_TIME"]], [0, 1], color="grey",
                     label="event")

        time_event = np.arange(0,
                               int(pat_cov["EVENT_NC_TIME"]) + 3.,
                               0.1)
        plt.plot(time_event, self.simulator.density(time_event,
                                                            np.exp(-self.simulator.param_event['nu'][0]),
                                                            np.exp(self.simulator.param_event['rho'][0]),
                                          pat_cov["E_XI"] - self.simulator.param_rm['xi_mean'],
                                          pat_cov["E_TAU"],
                                          self.simulator.param_rm['tau_mean']),
                 label=f"real {round(pat_cov['E_XI'], 2)}, {round(pat_cov['E_TAU'], 2)}", color="green")
        plt.plot(time_event, self.simulator.density(time_event,
                                                            np.exp(-self.simulator.param_event['nu'][0]),
                                                            np.exp(self.simulator.param_event['rho'][0]),
                                                    0,
                                          self.simulator.param_rm['tau_mean'],
                                          self.simulator.param_rm['tau_mean']),
                 label=f"real_median ", color="blue")

        plt.legend()
        plt.ylim([0, 0.3])
        plt.ylabel("score")
        plt.xlabel("time")
        plt.title(f'Patient {id_pat}')
        plt.savefig(f"{path_pat}{id_pat}_e.png")
        plt.show()



