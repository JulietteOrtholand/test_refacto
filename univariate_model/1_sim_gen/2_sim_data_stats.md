```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.style.use("../utils/plot/matplotlibrc.txt")
```


# Get data

```python
dict_scen = {"Patients (P)":[f'SIM_P_{i}' for i in [1,2,3]],
             "Longitudinal (L)": [f'SIM_L_1', f'SIM_L_2', f'SIM_P_3'],
             "Visits (V)": [f'SIM_V_1', f'SIM_P_3', f'SIM_V_3'],
             "Follow-up (F)": [f'SIM_F_1', f'SIM_P_3', f'SIM_F_3'],
             "Survival (S)": [f'SIM_S_1', f'SIM_P_3', f'SIM_S_3'],
            }
```


```python
dict_data = {}
for key, names in dict_scen.items():
    dict_data[key] = []
    for data_name in names:
        dict_data[key].append(pd.read_csv(f"../_data/{data_name}_data.csv"))
        
```


# Key statistics

```python
list_len = []
for key, data in dict_data.items():
    if key != "Patients (P)":
        list_len+=[len(df.groupby('ID').first()) for df in data]
"patients",round(np.array(list_len).mean(),0), round(np.array(list_len).std(),0)
```


```python
list_len = []
for key, data in dict_data.items():
    if key not in ["Patients (P)","Visits (V)","Follow-up (F)"]:
        list_len+=[len(df) for df in data]
"visits",round(np.array(list_len).mean(),0), round(np.array(list_len).std(),0)
```


# Plot

```python
dict_ylim = {'median ': [(-0.2, 3.),"year" ],
             'growth ': [(-0.15, 0.01), "per year"],
             'midpoint ': [(-0.2, 1.4),"year"],
                         }
from matplotlib.ticker import FormatStrFormatter

fig, ax = plt.subplots(1,3,figsize=(15, 6), sharey = False)

for j in range(3):
    
    ax[j].set_xticks([0,1,2], ["Easy", "Medium", "Hard"])
    
    if j == 1:
        for key, datas in dict_data.items():
            ax[j].plot([2, 1, 0], [len(df) for df in datas], label = key, marker="o",)
        
        ax[j].set_ylabel('Number total of visits')
        ax[j].set_title('Visits generated')

        
    elif j == 0:
        for key, datas in dict_data.items():
            ax[j].plot([2, 1, 0], [len(df.groupby('ID').first()) for df in datas], label = key, marker="o",)
        ax[j].set_ylabel('Number of patients')
        ax[j].set_title('Patients generated')
        
    elif j == 2:
        for key, datas in dict_data.items():
            list_val = []
            for df in datas:
                df_int = df.groupby('ID').first()['EVENT_BOOL']
                list_val.append(df_int.value_counts().loc[False]*100/len(df_int))

            ax[j].plot([2, 1, 0], list_val, label = key, marker="o",)
        ax[j].set_ylabel('Percentage of censored events')
        ax[j].set_title('Censored events')
        ax[j].legend(loc='center left', bbox_to_anchor=(1, 0.5))
    else:
        raise
        
fig.supxlabel('Study design level')
plt.savefig(f"simulated_analysis_graph.png",bbox_inches='tight')
plt.show()
```


```python

```


```python

```
