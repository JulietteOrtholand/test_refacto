```python
import pandas as pd
import numpy as np
import os
path_tools = os.path.join(os.getcwd(),'..','utils/data_extraction/')
import sys
sys.path.append(path_tools)

from event_analysis import EventAnalysis

%load_ext autoreload
%autoreload 2
```

# BENCHMARK on simulated data


```python
df_s = pd.read_csv("../_data/SIM_P_3_data.csv")
df_s["ID_SORT"] = df_s["ID"]
df_s["ID"] = df_s["ID"]#.astype("str")
df_s = df_s.set_index(["ID"])
df_s = df_s.rename(columns = {'EVENT_NC_TIME': 'EVENT_T_TIME',
                  'EVENT_NC_BOOL': 'EVENT_T_BOOL'})

df_s.reset_index().set_index(["ID", "TIME"])
print(f"{len(df_s)} visits for  {len(df_s.groupby('ID').first())} patients")

# Subselect patients with visits nb
features = ['ALSFRS_R_TOTAL']

index_vis = df_s.groupby("ID").mean()[(df_s.groupby("ID").count()[features].min(axis = 1)>2)].index

df_s = df_s.reset_index()
df_s = df_s[df_s["ID"].isin(index_vis)].set_index(["ID", "TIME"])

print(f"{len(df_s)} visits for  {len(df_s.groupby('ID').first())} patients")
df_s.to_csv(f"../_data/SIM_BENCH_MARK_data.csv")

ip_all = df_s.index.get_level_values('ID').unique()
list_fold = list(np.array_split(ip_all, 10))

for i in range(len(list_fold)):
    train = df_s.loc[np.concatenate(list_fold[:i]+list_fold[i+1:])]
    train.to_csv(f"../_data/SIM_TRAIN_CV{i}_data.csv")
    
    test = df_s.loc[list_fold[i]]
    test = test.reset_index().sort_values(['ID_SORT', 'TIME']).set_index(['ID', 'TIME'])
    
    #Drop for longitudinal predictions
    index_pred_nok = test[(test.index.get_level_values('TIME')>train.index.get_level_values('TIME').max())].index
    print(f"[DROPPED] CV{i} Visits :{len(index_pred_nok)}, patients: {len(index_pred_nok.get_level_values('ID').unique())}")
    test = test.drop(index_pred_nok)
    test['EVENT_NC_TIME'] = test['EVENT_TIME']
    test['EVENT_NC_BOOL'] = test['EVENT_BOOL']
    
    # Select 2 first 
    test_2 = test.groupby("ID").head(2)
    test_2 = test_2.join(test_2.reset_index().groupby('ID').max()['TIME'])
    test_2['EVENT_TIME'] = test_2['TIME']
    test_2['EVENT_BOOL'] = False
    test_2 = test_2.drop(['TIME'], axis = 1)
    
    # Set test all 
    test['EVENT_TIME'] = np.nan
    test['EVENT_TIME'] = test_2['EVENT_TIME']
    test['EVENT_BOOL'] = False
    test['EVENT_TIME'] = test['EVENT_TIME'].ffill()
    assert (test['EVENT_TIME'].groupby('ID').first() == test_2['EVENT_TIME'].ffill().groupby('ID').first() ).all()
    
    # Drop for survival predictions
    id_pred_ok = test_2[test_2['EVENT_TIME']+2<train['EVENT_TIME'].max()].index.get_level_values('ID').unique()
    print(f'[DROPPED] CV{i} patients drop:', len(test_2.index.get_level_values('ID').unique()) - len(id_pred_ok))
    test_2 = test_2.loc[id_pred_ok]
    test = test.loc[id_pred_ok]
   
    
    test_2.to_csv(f"../_data/SIM_TEST_2_CV{i}_data.csv")
    test.to_csv(f"../_data/SIM_TEST_ALL_CV{i}_data.csv")
```


```python
df_perfect = df_s.copy()
#df_perfect = df_perfect.drop(['EVENT_TIME', 'EVENT_BOOL'], axis = 1)
#df_perfect = df_perfect.rename(columns = {'EVENT_NC_TIME': 'EVENT_TIME', 'EVENT_NC_BOOL': 'EVENT_BOOL'})
df_perfect.to_csv(f"../_data/SIM_PERFECT_data.csv")
```


```python

```

```python

```
