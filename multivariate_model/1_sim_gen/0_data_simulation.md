```python
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import copy
from sksurv.nonparametric import kaplan_meier_estimator
from lifelines import WeibullFitter, KaplanMeierFitter
#from sklearn.linear_model import LinearRegression
import pickle
import gzip
import os
import numpy as np
from lifelines import WeibullFitter, KaplanMeierFitter
from sksurv.metrics import brier_score, integrated_brier_score
import matplotlib.pyplot as plt
import pandas as pd
from sksurv.metrics import concordance_index_censored
from sklearn.linear_model import LinearRegression
import json
import torch
path_tools = os.path.join(os.getcwd(),'..','utils/simulate')
import sys
sys.path.append(path_tools)

from simulate_data_save import SimulateData
from check_simulate_data_save import CheckSimulateData


%load_ext autoreload
%autoreload 2
```

# Number of patients


```python
## Number of patients to generate

np.random.seed(1)
```

# Leaspy parameters


```python
with open(f"model_parameters.json") as infile:
            leaspy_param = json.load(infile)
        
with open(f"event_parameters.json") as infile:
            event_param = json.load(infile)
```


```python
leaspy_param
```

```python
# Simulation parameters from a logistic univariate leaspy model structure

value_name = ['ALSFRS_R_BULBAR',
  'ALSFRS_R_FINE_MOTOR',
  'ALSFRS_R_GROSS_MOTOR',
  'ALSFRS_R_RESPIRATORY']


print(leaspy_param)

survival_params = {'nu':event_param['nu'], #20
                  'rho':event_param['rho'],
                  'zeta':event_param['zeta']}
print(survival_params)

visits_params = {'pat_nb':1000,
                 'fv_mean' : 0.4, #OK
                 'fv_std' : 0.84, #OK
                 'tf_mean' : 0.96, #OK
                 'tf_std' : 0.5, #OK
                 'distv_mean' : 1.47/12, #OK # 1.
                 'distv_std' : .87/12, #OK # 6
                }



join_params = {'delta_event':None,
              }



total_params = {'repeated_measure':leaspy_param,
                'time_to_event':survival_params,
               'study':visits_params,
                'join':join_params,
               }
seed = 0
```


```python
np.array(leaspy_param["parameters"]['betas']).shape
```

```python
np.exp(-survival_params['nu'][0]), np.exp(survival_params['rho'][0])
```


```python
def compute_mode(nu,rho):
    return nu*((rho-1)/rho)**(1/rho)

nu = np.exp(-survival_params['nu'][0])
rho = np.exp(survival_params['rho'][0])
new_rho = 2

mode_2 = compute_mode(nu, rho)
mode_4 = compute_mode(nu,new_rho)

nu, mode_2, nu*mode_2/mode_4, compute_mode(nu*mode_2/mode_4,new_rho)
```


```python
dict_sim = {"P":{"1": {("study", 'pat_nb'): 200},
                     "2": {("study", 'pat_nb'): 500},
                     "3": {("study", 'pat_nb'): 1000},
                     },
            
                 "V":{"1": {("study", 'distv_mean'): 3./12.,},
                     "3": {("study", 'distv_mean'): 1./12.,},
                    },
           
                "F":{"1": {("study", 'tf_mean'): 0.5},
                     "3": {("study", 'tf_mean'): 2.},
                    },
            
                 "L":{"1": {('repeated_measure', 'noise_std'): [0.2, 0.2, 0.2, 0.25]},
                     "2": {('repeated_measure', 'noise_std'): [0.1, 0.1, 0.1, 0.15]},
                     },
            
                "S":{"1": {('time_to_event', 'rho'): [np.log(1.1)],
                          ('time_to_event', 'nu'): [-np.log(3.932)]},
                     "3": {('time_to_event', 'rho'): [np.log(2.)],
                            ('time_to_event', 'nu'): [-np.log(0.628)]},     
                    },
           
}
dict_sim
```


```python
for name_scenario, dict_scenario in dict_sim.items():
    for nb_scenario, values in dict_scenario.items():
        
        print(name_scenario, nb_scenario)
        
        
        # Update parameters
        params = copy.deepcopy(total_params)
        for (key1, key2), val in values.items():
            if key1 == "repeated_measure":
                params[key1]['parameters'][key2] = val
            else:
                params[key1][key2] = val
        
        # Create objects
        simulator = SimulateData(params, "NOISY_LINK", f"{name_scenario}_{nb_scenario}")
        checks = CheckSimulateData(simulator,"sim_checks/",seed)
        #simulator.simulate_data(0)
        # Make checks
        checks.check_all()
        # Save data
        
        df_out = checks.df_censored
        df_out['ID_SORT'] = df_out.index.get_level_values('ID')
        df_out.to_csv(f"../_data/SIM_{name_scenario}_{nb_scenario}_data.csv")
        
        df_out = checks.df_all
        df_out['EVENT_TIME'] = df_out['EVENT_NC_TIME']
        df_out['EVENT_BOOL'] = df_out['EVENT_NC_BOOL']
        df_out['ID_SORT'] = df_out.index.get_level_values('ID')
        df_out.to_csv(f"../_data/SIM_{name_scenario}_{nb_scenario}_ALL_data.csv")
        
```
```python

```
